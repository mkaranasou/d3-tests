$(document).ready(function() {

    // ------------------------------------------------------------------------------------
    // Declarations
    // ------------------------------------------------------------------------------------
    var el = $(".serieschart");//selector
    var corelabels;
    var dropShadowFilter, intenseShadow;
    var width = el.data("width");
    var height = el.data("height");
    var margin = {
        top: 65,
        right: 90,
        bottom: 5,
        left: 150
    };
    var items = [];
    var layerData = [];
    var distancesL0 = [];
    var distancesL1 = [];


    var rawData = [{
        "name": "Twitter",
        "vists": "15 billion",
        "unit": "per day",
        "layer1value": 1500000,
        "layer2value": 15450
    }, {
        "name": "Facebook",
        "vists": "5 billion",
        "unit": "per day",
        "layer1value": 5000000,
        "layer2value": 2000
    }, {
        "name": "Google",
        "vists": "5 billion",
        "unit": "per day",
        "layer1value": 50000,
        "layer2value": 35000
    }, {
        "name": "Netflix",
        "vists": "10 billion",
        "unit": "per day",
        "layer1value": 3000000,
        "layer2value": 2200
    }, {
        "name": "Ebay",
        "vists": "8 billion",
        "unit": "per day",
        "layer1value": 2500000,
        "layer2value": 17700
    }, {
        "name": "Klout",
        "vists": "2 billion",
        "unit": "per day",
        "layer1value": 1000000,
        "layer2value": 45
    }];

    var dd1 = [];
    var dd2 = [];

    $.each(rawData, function(k, v){
        dd1.push(v["layer1value"]);
        dd2.push(v["layer2value"]);
    });

    var meansPerGroup = [Math.max.apply(null, dd1), Math.max.apply(null, dd2)];

    function getCircleSize(j, key, data) {

        function getPercentage(value, total) {
            return ((value * 100 )/ total);
        }

        var layerArray = [];
        var total = 0;

        $.each(data, function(index, value) {
            layerArray.push(value[key]);
            total += value[key];
        });

        var layerSet = [];
        var ratios = [60, 25];
        for (var i = 0; i < layerArray.length; i++) {
            layerSet.push(parseInt(getPercentage(layerArray[i], meansPerGroup[j-1])*ratios[j-1], 10));
        }
        return layerSet;
    }

    function getCoreData(d) {

        var returneddata = [];
        for (var i = 1; i <= 2; i++) {
            returneddata.push(getCircleSize(i, "layer" + i + "value", d));
        }

        return returneddata;
    }

    var coredata = getCoreData(rawData);

    function getLayerLabel(key, d) {
        var layer = [];

        $.each(d, function(index, value) {
            layer.push(value[key]);
        });

        return layer;
    }


    function getCoreLabels(d) {
        var corelabels = [];
        var keys = ["name", "layer2value", "vists", "unit"];

        for (var i = 0; i < 4; i++) {
            corelabels.push(getLayerLabel(keys[i], d));
        }
        return corelabels;
    }



    // ------------------------------------------------------------------------------------
    // Function Declarations
    // ------------------------------------------------------------------------------------

    // ShadowFilters
    var createShadowDefs = function(defs) { //http://bl.ocks.org/wimdows/1502762
        defs.append('svg:linearGradient')
            .attr('gradientUnits', 'userSpaceOnUse')
            .attr('x1', 0).attr('y1', 0).attr('x2', 20).attr('y2', 0)
            .attr('id', 'master').call(
            function(gradient) {
                gradient.append('svg:stop').attr('offset', '0%').attr('style', 'stop-color:white;stop-opacity:1');
                gradient.append('svg:stop').attr('offset', '100%').attr('style', 'stop-color:#3498db;stop-opacity:1');
            });
        // create regular shadow with small footprint
        dropShadowFilter = defs.append('svg:filter')
            .attr('id', "drop-shadow")
            .attr('filterUnits', "userSpaceOnUse")
            .attr('width', '110%')
            .attr('height', '110%');
        dropShadowFilter.append('svg:feGaussianBlur')
            .attr('in', 'SourceGraphic')
            .attr('stdDeviation', 2)
            .attr('type', 'hueRotate')
            .attr('values', 240)
            .attr('result', 'blur-out');
        dropShadowFilter.append('svg:feColorMatrix')
            .attr('in', 'blur-out')
            .attr('type', 'hueRotate')
            .attr('values', 180)
            .attr('result', 'color-out');
        dropShadowFilter.append('svg:feOffset')
            .attr('in', 'color-out')
            .attr('dx', 1)
            .attr('dy', 1)
            .attr('result', 'the-shadow');
        dropShadowFilter.append('svg:feBlend')
            .attr('in', 'SourceGraphic')
            .attr('in2', 'the-shadow')
            .attr('mode', 'normal');

        // create intense shadow defs for hovering
        intenseShadow = defs.append('svg:filter')
            .attr('id', 'intenseShadow')
            .attr('filterUnits', "userSpaceOnUse")
            .attr('width', '130%')
            .attr('height', '130%');
        intenseShadow.append('svg:feGaussianBlur')
            .attr('in', 'SourceGraphic')
            .attr('stdDeviation', 2)
            .attr('result', 'blur-out');
        intenseShadow.append('svg:feColorMatrix')
            .attr('in', 'blur-out')
            .attr('type', 'hueRotate')
            .attr('values', 180)
            .attr('result', 'color-out');
        intenseShadow.append('svg:feOffset')
            .attr('in', 'color-out')
            .attr('dx', 5)
            .attr('dy', 5)
            .attr('result', 'the-shadow');
        intenseShadow.append('svg:feBlend')
            .attr('in', 'SourceGraphic')
            .attr('in2', 'the-shadow')
            .attr('mode', 'normal');

    }

    // calculates where each element should be placed
    var calculateDistance = function(d, items) {
        var dcx = 0;
        for (var k = 0; k < d.j; k++) {
            dcx += Math.sqrt(items[k].value);
        }
        return dcx + 10 * d.j;
    }

    function addData(dataArray, itemsArray, fn) {
        $.each(dataArray, function(layer, value) {
            $.each(value, function(j, v) {
                var obj = {
                    "l": layer,
                    "j": j,
                    "value": v
                };
                itemsArray.push(obj);
            });

        });
        console.log("itemsArray", itemsArray);
        fn && fn(itemsArray);
    }

    // Create Circles
    function setCircles(items) {
        console.log("ITEMS BEFORE", items);
        // sort elements in order to draw them by size
        items.sort(function(a, b) {
            return parseInt(b.value) - parseInt(a.value);
        });

        console.log("ITEMS AFTER", items);
        var circlelayer = svg.append("g")
            .attr("class", "circlelayer");

        var circle = circlelayer.selectAll("circle")
            .data(items);

        circle.enter().append("circle")
            .attr("class", function(d, i) {
                if (d.l == 0) {
                    return "blue";
                }
                return "gold";
            })
            .attr("cy", 60)
            .attr("cx", function(d, i) {
                var distance = calculateDistance(d, items);
                if (d.l == 1) {
                    distancesL1.push(distance);
                } else {
                    distancesL0.push(distance);
                }
                return distance;
            })
            .attr("r", function(d, i) {
                return Math.sqrt(d.value);
            })
            .attr("filter", function(d) {
                return "url(#drop-shadow)";
            });
        circle.enter()
            .append("text")
            .text(function(d){return d.value})
            .attr("x", function(d, i){return calculateDistance(d, items);})
            .attr("y", function(d, i){return d.l * 50})
        circle.exit().remove();
    }

    // Create Labels
    function setLabels(layerData) {
        // add pointer and label elements afterwards so they display on top
        //__pointers
        var pointerpattern = svg.append("g")
            .attr("class", "pointerpattern");

        pointerpattern.append("defs").append("marker")
            .attr("id", "circ")
            .attr("markerWidth", 6)
            .attr("markerHeight", 6)
            .attr("refX", 3)
            .attr("refY", 3)
            .append("circle")
            .attr("cx", 3)
            .attr("cy", 3)
            .attr("r", 3)
            .style("fill", "transparent");

        var labelslayer = svg.append("g")
            .attr("class", "labelslayer");

        var pointerlayer = svg.append("g")
            .attr("class", "pointerlayer");

        var labels = labelslayer.selectAll("text")
            .data(layerData);

        labels.enter()
            .append("text")
            .attr("class", function(d) {
                return "label-layer" + d.l;
            })
            .attr('fill', function(d, i) {
                return 'url(#gradient' + i + ')';
            })
            .attr("transform", function(d, i) {

                switch (d.l) {
                    case 1:
                        return "translate(" + (15 * i) + "," + (d.j * 45) + ") rotate(-55)";
                        break;
                    case 2:
                        return "translate(90," + (d.j * -30) + ") rotate(25)";
                        break;
                    case 3:
                        return "translate(60," + (d.j * -30) + ") rotate(25)";
                        break;
                    default:
                        return "translate(0,0) rotate(0)";
                }
            })
            .attr("text-anchor", "middle");


        labels
            .attr("cy", function(d, i) {

                d.cy = 60;
                switch (d.l) {
                    case 1:
                        return 140;
                        break;
                    case 2:
                        return 120;
                        break;
                    case 3:
                        return 120;
                        break;
                    default:
                        return -20;
                }

            })
            .attr("cx", function(d, i) {

                d.cx = d.l == 1 ? distancesL1[d.j] : distancesL0[d.j] //distance; //(d.j * 60) + 10;
                return d.l == 1 ? distancesL1[d.j] : distancesL0[d.j];
            })
            .attr("x", function(d, i) {
                var distance = calculateDistance(d, items);

                switch (d.l) {
                    case 0:
                        d.x = distance + Math.sqrt(d.j) * i;
                        return distance + Math.sqrt(d.j) * i;
                    case 1:
                        if (d.j == 0) distance += 20;

                        d.x = distance + Math.sqrt(d.j) * i;
                        return distance + Math.sqrt(d.j) * i;

                        break;
                    case 2:

                       if (d.j == 0) distance += 20;

                        d.x = distance + Math.sqrt(d.j) * i;
                        return distance + Math.sqrt(d.j) * i;

                        break;
                    case 3:

                       if (d.j == 0) distance += 20;

                        d.x = distance + Math.sqrt(d.j) * i;
                        return distance + Math.sqrt(d.j) * i;

                        break;
                    default:
                        d.x = distancesL0[d.j]; //(d.j * 60) + 10;
                        return distancesL0[d.j]; //(d.j * 60) + 10;
                }

            })
            .attr("y", function(d, i) {

                switch (d.l) {
                    case 1:
                        var distance = -20;
                        //console.log(i, d.j, distance)
                        d.y = distance;
                        return d.y;

                        break;
                    case 2:
                        var distance = 120;
                        //console.log(i, d.j, distance)
                        d.y = distance;
                        return d.y;

                        break;
                    case 3:
                        var distance = 120;
                        //console.log(i, d.j, distance)
                        d.y = distance;
                        return d.y;

                        break;
                    default:
                        d.y = 150;
                        return 150;
                }


            })
            .text(function(d) {
                return d.value;
            })
            .each(function(d) {
                var bbox = this.getBBox();
                d.sx = d.x - bbox.width / 2 - 2;
                d.ox = d.x + bbox.width / 2 + 2;
                d.sy = d.oy = d.y + 5;
            })
            .transition()
            .duration(300)

        labels
            .transition()
            .duration(300)

        labels.exit()
            .transition()
            .duration(300)
        //__labels

        var pointers = pointerlayer.selectAll("path.pointer")
            .data(layerData);

        pointers.enter()
            .append("path")
            .attr("class", function(d) {
                return "pointer" + d.l;
            })
            .attr("marker-end", "url(#circ)");

        pointers
            .attr("d", function(d) {
                if (d.l == 1) {
                    return "M" + (distancesL1[d.j] + 70) + "," + (d.y + d.oy) + "L" + d.cx + "," + d.cy + " " + d.cx + "," + d.cy;
                }
                // start at the center with d.x - x value stays the same throughout the line - and d.y - 15 to go just above the letters
                return "M" + distancesL0[d.j] + "," + (d.y - 15) + "L" + distancesL0[d.j] + "," + (d.cy)

                /* if (d.cx > d.ox) {
                 return "M" + d.x + "," + d.y + "L" + d.x + "," + d.y + " " + d.cx + "," + d.cy;
                 } else {

                 return "M" + (distancesL0[d.j]) + "," + (d.oy - 15) + "L" + (distancesL0[d.j]) + "," + (d.sy - 15) + " " + distancesL0[d.j] + "," + (d.cy + Math.sqrt(distancesL0[d.j]));
                 }*/
            })
            .transition()
            .duration(300)

        pointers
            .transition()
            .duration(300)

        pointers.exit()
            .transition()
            .duration(300)

        //__pointers

    }


    // ------------------------------------------------------------------------------------
    // Main body
    // ------------------------------------------------------------------------------------

    // Set the main elements for the series chart
    var svg = d3.select(el[0]).append("svg")
        .attr("class", "series")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var defs = svg.append("svg:defs");
    createShadowDefs(defs);
    // initialize data array for circle data
    addData(coredata, items, setCircles);

    var corelabels = getCoreLabels(rawData);
    // initialize data array for pointers and label data
    addData(corelabels, layerData, setLabels);

    defs.selectAll('.gradient').data(layerData)
        .enter()
        .append('svg:linearGradient')
        .attr('id', function(d, i) {
            return 'gradient' + i;
        })
        .attr('class', 'gradient')
        .attr('xlink:href', '#master')
        .attr('gradientTransform', function(d) {
            return 'translate(' + d.j + 5 + ')';
        });

});
